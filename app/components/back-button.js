import Ember from 'ember';

export default Ember.Component.extend({

	hasHistory: false,

	init:function(){
		this._super();
		if(!window.history || !window.history.length){
			this.set('class', 'disabled');
		}
	},

	actions: {
		back: function(){
			window.history.back();
		}
	}

});
