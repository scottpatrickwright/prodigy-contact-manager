import Ember from 'ember';

export default Ember.Controller.extend({
	/* settings controller stub */

	actions: {
		save: function(){
			this.transitionToRoute('contacts');
		},
		cancel: function(){
			this.transitionToRoute('contacts');
		}
	}

});
