import Ember from 'ember';

export default Ember.Controller.extend({
	
	//TODO this could be a mixin - also not a proper GUID
	guid: function(){
		
		function s4() {
		    return Math.floor((1 + Math.random()) * 0x10000)
		      .toString(16)
		      .substring(1);
		  }
		  
		  return s4();
	},


	actions: {
		save: function() {
			
			//persist form data to store
			
			var _model = this.get('model');
			var formAction = this.get('formAction');
			
			if(formAction === 'add'){
				_model.set('id', this.guid());	
			}
			
			_model.save(); //TODO causes error in Mirage - empty handler
			
			switch(formAction){

				case 'add':
				this.transitionToRoute('contacts');				
				break;

				case 'edit':
				this.transitionToRoute('contact-view', _model);
				break;

			}

		},
		cancel: function() {
			//rollback form data
			var _model = this.get('model');
			var formAction = this.get('formAction');

			this.get('model').rollbackAttributes();
			
			switch(formAction){

				case 'add':
				this.transitionToRoute('contacts');				
				break;

				case 'edit':
				this.transitionToRoute('contact-view', _model);
				break;

			}
		}
	}

});
