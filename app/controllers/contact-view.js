import Ember from 'ember';

export default Ember.Controller.extend({

  actions: {
  	edit: function(){
  		var contact = this.get('model');
  		this.transitionToRoute('contact-edit', contact);
  	}
  }

});
