import Ember from 'ember';

export default Ember.Route.extend({

	controllerName: 'contact-edit',
   	templateName: 'contact-edit',

   	model() {
    	return this.store.createRecord('contact', null);
  	},

   	setupController: function(controller, model) {
		this._super(controller, model);
		controller.set('formAction', 'add');
	}
});

