import Ember from 'ember';

export default Ember.Route.extend({

	formAction: "edit", //possible values are edit & add

	model(params) {
    	return this.store.findRecord('contact', params.id);
  	},

  	setupController: function(controller, model) {
		this._super(controller, model);
		controller.set('formAction', 'edit');
	},

  	deactivate: function(){
  		//if the transaction has already been committed or rolled back, this will have no effect
  		this.controller.model.rollbackAttributes();
  	}
  	
});
