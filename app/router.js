import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('contacts');
  this.route('contact-view', { path: '/contact-view/:id' });
  this.route('contact-edit', { path: '/contact-edit/:id' });
  this.route('contact-add');
  this.route('settings');
});

export default Router;
