import DS from 'ember-data';

export default DS.Model.extend({
  email: DS.attr(),
  telephone: DS.attr(),
  department: DS.attr(),
  firstname: DS.attr(),
  lastname: DS.attr(),
  employeeid: DS.attr()
});