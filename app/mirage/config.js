export default function() {
  
  var contactsData =
    [{
        type: 'contacts',
        id: 1,
        attributes: {
          email: 'john@company.com',
          telephone: '416-555-1111',
          department: 'finance',
          firstname: 'John',
          lastname: 'Smith',
          employeeid: 100
        }
      }, {
        type: 'contacts',
        id: 2,
        attributes: {
          email: 'val@company.com',
          telephone: '416-555-2222',
          department: 'warehouse',
          firstname: 'Valerie',
          lastname: 'Anderson',
          employeeid: 101
        }
      }, {
        type: 'contacts',
        id: 3,
        attributes: {
          email: 'bobby@company.com',
          telephone: '416-555-3333',
          department: 'sales',
          firstname: 'Bobby',
          lastname: 'Smith',
          employeeid: 102

        }
      },
      {
        type: 'contacts',
        id: 4,
        attributes: {
          email: 'lisa@company.com',
          telephone: '416-555-4444',
          department: 'finance',
          firstname: 'Lisa',
          lastname: 'Green',
          employeeid: 103
        }
      }];

  this.get('/contacts', function() {
    return {
      data: contactsData
    };
  });
  
  this.get('/contacts/:id', function(db, request) {
   
    var id = Number(request.params.id);
   
    return { 
      data: contactsData.find(function(contact){
        return contact.id === id;
      })
    };
  });

  //this.patch('/contacts/:id', 'contacts'); // specify the type of resource to be updated

  this.patch('/contacts/:id', function(db, request){

      console.log('patch contact ' + request.params.id);

      var id = Number(request.params.id);

      var idx = contactsData.findIndex(function(contact){
        return contact.id === id;
      });

      console.dir(request.requestBody);

      contactsData[idx] = request.requestBody.data; //TODO throws error

  });

  this.post('/contacts', 'contacts'); 
}