import { test } from 'qunit';
import moduleForAcceptance from 'prodigy-contacts-manager/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | contact add');

	test('visiting /contact-add', function(assert) {
	    
	    //TEST saved contact is displayed

	    visit('/contact-add');

	    andThen(function() {
	        assert.equal(currentURL(), '/contact-add');
	    });

	    fillIn('input[placeholder="First Name"]', 'ABC 123 Test Name');
	    fillIn('input[placeholder="Last Name"]', 'Test Last name');
	    fillIn('input[placeholder="Email"]', 'Test Email');
	    fillIn('input[placeholder="Telephone"]', 'Test Telephone');
	    fillIn('input[placeholder="Department"]', 'Test Department');
	    click('button[value="Save"]');

	    visit('/contacts');

	    andThen(function() {
	    	assert.equal(find("td:contains(ABC 123 Test Name)").length, 1, "saved contact is displayed");
	    });

	    //TEST cancelled contact is not displayed

	    visit('/contact-add');

	    andThen(function() {
	        assert.equal(currentURL(), '/contact-add');
	    });

	    fillIn('input[placeholder="First Name"]', 'DEF 123 Test Name');
	    fillIn('input[placeholder="Last Name"]', 'DEF Last name');
	    fillIn('input[placeholder="Email"]', 'DEF Email');
	    fillIn('input[placeholder="Telephone"]', 'DEF Telephone');
	    fillIn('input[placeholder="Department"]', 'DEF Department');
	    click('button[value="Cancel"]');

	    visit('/contacts');

	    andThen(function() {
	    	assert.equal(find("td:contains(DEF 123 Test Name)").length, 0, "cancelled contact is not displayed");
	    });

	    //TEST contact is not displayed when data is entered but user navigates away from form without canceling OR saving

	    visit('/contact-add');

	    andThen(function() {
	        assert.equal(currentURL(), '/contact-add');
	    });

	    fillIn('input[placeholder="First Name"]', 'GHI 123 Test Name');
	    fillIn('input[placeholder="Last Name"]', 'GHI Last name');
	    fillIn('input[placeholder="Email"]', 'GHI Email');
	    fillIn('input[placeholder="Telephone"]', 'GHI Telephone');
	    fillIn('input[placeholder="Department"]', 'GHI Department');
	    
	    visit('/contacts');

	    andThen(function() {
	    	assert.equal(find("td:contains(GHI 123 Test Name)").length, 0, "cancelled contact is not displayed");
	    });

    });